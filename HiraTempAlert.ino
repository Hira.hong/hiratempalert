#include <ESP8266WiFi.h>
#include <Thermocouple.h>
#include <MAX6675_Thermocouple.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

#ifndef STASSID
#define STASSID "ssid"
#define STAPSK  "psk"
#endif

#define piezoPin D1
#define ledPin D6

const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "http://b1ad478a.ngrok.io/temperature/";

/*
  Average Thermocouple

  Reads a temperature from a thermocouple based
  on the MAX6675 driver and displays it in the default Serial.

  https://github.com/YuriiSalimov/MAX6675_Thermocouple

  Created by Yurii Salimov, May, 2019.
  Released into the public domain.
*/

#define SCK_PIN D5
#define CS_PIN  D7
#define SO_PIN  D8

Thermocouple* thermocouple;

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  thermocouple = new MAX6675_Thermocouple(SCK_PIN, CS_PIN, SO_PIN);

  pinMode(piezoPin, OUTPUT); //buzzer
  pinMode(ledPin, OUTPUT); //led indicator when beeping
}

void loop() {
  const double celsius = thermocouple->readCelsius();
  const double kelvin = thermocouple->readKelvin();
  const double fahrenheit = thermocouple->readFahrenheit();

  Serial.println(fahrenheit);
  
  if (fahrenheit > 81) {
    makeRequest(fahrenheit);
  }

  delay(10000);  //Send a request every 10 seconds
}

void makeRequest(double fahrenheit) {
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;    //Declare object of class HTTPClient

    http.begin(host);      //Specify request destination
    http.addHeader("Content-Type", "application/json");  //Specify content-type header

    StaticJsonBuffer<300> JSONbuffer;
    JsonObject& JSONencoder = JSONbuffer.createObject();

    JSONencoder["temperature"] = String(fahrenheit);


    char JSONmessageBuffer[300];
    JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    Serial.println(JSONmessageBuffer);
    int httpCode = http.POST(JSONmessageBuffer);   //Send the request
    String payload = http.getString();                  //Get the response payload

    Serial.println(payload);    //Print request response payload

    http.end();  //Close connection

  } else {
    Serial.println("Error in WiFi connection");
  }
}

void beep() {
  digitalWrite(ledPin, HIGH);
  digitalWrite(piezoPin, HIGH);
  delay(1000);
  digitalWrite(ledPin, LOW);
  digitalWrite(piezoPin, LOW);
}
