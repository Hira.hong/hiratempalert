# Hira Temp Alert

**An esp8266 based iot project** - monitor and alert based on temperature levels

The current pin out and configuration is based on using the ESP8266 - NodeMCU 1.0 ESP-12E

* Code Editor: https://www.arduino.cc/en/Main/Software
* Information on adding esp8266 to board list: https://randomnerdtutorials.com/how-to-install-esp8266-board-arduino-ide/
* MAX6675 Board Library - https://github.com/YuriiSalimov/MAX6675_Thermocouple